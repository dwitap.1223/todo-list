import { createSlice } from '@reduxjs/toolkit';

const todosSlice = createSlice({
  name: 'todos',
  initialState: [],
  reducers: {
    addTodo: (state, action) => [...state, { id: Date.now(), text: action.payload, completed: false }],
    removeTodo: (state, action) => state.filter((todo) => todo.id !== action.payload),
    toggleActice: (state, action) => {
      const todo = state.find((todo) => todo.id === action.payload);
      if (todo) {
        todo.completed = !todo.completed;
      }
    },
    clearTodo: (state) => state.filter((todo) => !todo.completed),
    editTodo: (state, action) => {
      const { id, text } = action.payload;
      const todoToEdit = state.find((todo) => todo.id === id);
      if (todoToEdit) {
        todoToEdit.text = text;
      }
    },
  },
});

export const { addTodo, removeTodo, toggleActice, clearTodo, editTodo } = todosSlice.actions;

export default todosSlice.reducer;

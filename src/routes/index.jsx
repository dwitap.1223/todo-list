import MainLayout from '@layouts/MainLayout';

import Home from '@pages/Home';
import NotFound from '@pages/NotFound';
import ToDo from '@pages/Todo';

const routes = [
  {
    path: '/',
    name: 'TodoPage',
    component: ToDo,
  },
  { path: '*', name: 'Not Found', component: NotFound, layout: MainLayout },
];

export default routes;

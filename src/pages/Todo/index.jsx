import * as React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useState } from 'react';
import classes from './style.module.scss';

import LightModeIcon from '@mui/icons-material/LightMode';
import DarkModeIcon from '@mui/icons-material/DarkMode';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import ClearIcon from '@mui/icons-material/Clear';
import EditIcon from '@mui/icons-material/Edit';
import Modal from '@mui/material/Modal';
import TextField from '@mui/material/TextField';

import { darkTheme, lightTheme } from '../../redux/themeSlice';
import { addTodo, clearTodo, removeTodo, toggleActice, editTodo } from '../../redux/todoSlice';

const Todo = () => {
  const dispatch = useDispatch();
  const themes = useSelector((state) => state.theme.theme);
  const todos = useSelector((state) => state.todos);

  const [open, setOpen] = React.useState(false);
  const handleClose = () => setOpen(false);

  const [task, setTask] = useState('');
  const [filter, setFilter] = useState('all');
  const [selectedId, setSelectedId] = useState('');
  const [editedText, setEditedText] = useState('');

  const handleSubmit = (e) => {
    e.preventDefault();
    if (task.trim() !== '') {
      dispatch(addTodo(task));
      setTask('');
    }
  };

  const handleEnter = (e) => {
    if (e.key === 'Enter') {
      e.preventDefault();
      handleSubmit(e);
    }
  };

  const handleRemove = (id) => {
    dispatch(removeTodo(id));
  };

  const handleCheck = (id) => {
    dispatch(toggleActice(id));
  };

  const handleClear = () => {
    dispatch(clearTodo());
  };

  const handleEdit = (item) => {
    setEditedText(item.text);
    setSelectedId(item.id);
    setOpen(true);
  };

  const handleSaveEdit = () => {
    if (editedText.trim() !== '') {
      dispatch(editTodo({ id: selectedId, text: editedText }));
      setOpen(false);
    }
  };

  const handleEditedTextChange = (e) => {
    setEditedText(e.target.value);
  };

  const filteredTodo = todos.filter((todo) => {
    if (filter === 'active') {
      return !todo.completed;
    }
    if (filter === 'completed') {
      return todo.completed;
    }
    return true;
  });

  return (
    <div className={`${classes.container} ${classes[themes]}`}>
      <div className={classes.contentContainer}>
        <div className={classes.head}>
          <h1 className={classes.div1}>TODO</h1>
          {/* === thema */}
          <div className={classes.div2}>
            {themes === 'dark' ? (
              <LightModeIcon onClick={() => dispatch(lightTheme({ theme: 'light' }))} />
            ) : (
              <DarkModeIcon onClick={() => dispatch(darkTheme({ theme: 'dark' }))} />
            )}
          </div>
        </div>
        {/* ==== input */}
        <div className={classes.div3}>
          <input
            type="text"
            value={task}
            id="input-box"
            onChange={(e) => setTask(e.target.value)}
            onKeyPress={handleEnter}
            placeholder="Create new to do list"
            className={classes.input}
          />
        </div>
        <div className={classes.box}>
          {/* ==== list todo */}
          <div className={classes.div4}>
            <List sx={{ width: '100%' }} className={classes.list}>
              {filteredTodo.length === 0 ? (
                <div className={classes.noData}>Please fill your todo list</div>
              ) : (
                filteredTodo.map((item) => (
                  <ListItem key={item.id} disablePadding sx={{ borderBottom: '2px solid gray' }}>
                    <ListItemButton>
                      <ListItemIcon>
                        <input
                          type="checkbox"
                          className={classes.checkbox}
                          checked={item.completed}
                          onChange={() => handleCheck(item.id)}
                        />
                      </ListItemIcon>
                      <ListItemText
                        primary={item.text}
                        className={item.completed === true ? classes.checkActive : null}
                      />
                      <EditIcon onClick={() => handleEdit(item)} sx={{ fontSize: 'large' }} />
                      <ClearIcon onClick={() => handleRemove(item.id)} />
                    </ListItemButton>
                  </ListItem>
                ))
              )}
            </List>
          </div>
          {/* ==== footer */}
          <div className={classes.footer}>
            <div>{filteredTodo.length} item left</div>
            {/* ==== footer desktop */}
            <div className={classes.footer1}>
              <div onClick={() => setFilter('all')} className={filter === 'all' ? classes.active : null}>
                All
              </div>
              <div onClick={() => setFilter('active')} className={filter === 'active' ? classes.active : null}>
                Active
              </div>
              <div onClick={() => setFilter('completed')} className={filter === 'completed' ? classes.active : null}>
                Completed
              </div>
            </div>
            <div onClick={handleClear}>Clear Completed</div>
          </div>
        </div>
        {/* ==== footer mobile */}
        <div className={classes.footer2}>
          <div onClick={() => setFilter('all')} className={filter === 'all' ? classes.active : null}>
            All
          </div>
          <div onClick={() => setFilter('active')} className={filter === 'active' ? classes.active : null}>
            Active
          </div>
          <div onClick={() => setFilter('completed')} className={filter === 'completed' ? classes.active : null}>
            Completed
          </div>
        </div>
        <div className={classes.bottom}>Drag and drop to reorder list</div>
      </div>

      <Modal open={open} onClose={handleClose}>
        <div className={classes.modalContainer}>
          <div className={classes.modal}>
            <TextField
              id="outlined-basic"
              label="Edit to do list"
              variant="outlined"
              type="text"
              value={editedText}
              onChange={handleEditedTextChange}
            />
            <div>
              <button onClick={handleSaveEdit} className={classes.buttonSave}>
                Save
              </button>
              <button onClick={handleClose} className={classes.buttonCancel}>
                Cancel
              </button>
            </div>
          </div>
        </div>
      </Modal>
    </div>
  );
};

export default Todo;

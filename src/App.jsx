import React from 'react';
import { DndProvider } from 'react-dnd';
import { HTML5Backend } from 'react-dnd-html5-backend';

import Todo from '@pages/Todo';

function App() {
  return (
    <DndProvider backend={HTML5Backend}>
      <Todo />
    </DndProvider>
  );
}

export default App;
